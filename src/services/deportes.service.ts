import { Api } from "./deportesAPi";

export class DeportesService {
  private http = Api.Http;
  private apikey="bc649e20-3a85-11ed-b153-8fdda65539af"

  public async getPais() {
    return await this.http.get(`/soccer/countries?apikey=${this.apikey}`).then((res) => res.data);
  }

  public async getEquipos(id:any) {
    return await this.http.get(`/soccer/teams?apikey=${this.apikey}&country_id=${id}`).then((res) => res.data);
  }

  /*public async getWorldCup() {
    return await this.http.get(`soccer/seasons?apikey=${this.apikey}&league_id=788`).then((res) => res.data);
  }
  public async getWorldCupQualificationWomen() {
    return await this.http.get(`soccer/seasons?apikey=${this.apikey}&league_id=782`).then((res) => res.data);
  }*/

  public async getBundesliga() {
    return await this.http.get(`soccer/seasons?apikey=${this.apikey}&league_id=314`).then((res) => res.data);
  }
  public async getMatches(season_id:any,from:any) {
    return await this.http.get(`soccer/matches?apikey=${this.apikey}&season_id=${season_id}&date_from=${from}`).then((res) => res.data);
  }
}
